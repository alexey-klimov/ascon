//
//  main.cpp
//  Ascon
//
//  Created by Alexey Klimov on 08.08.16.
//  Copyright © 2016 Alexey Klimov. All rights reserved.
//
// ------------------------------------------------------
// Насколько я понял, вы просили не использовать STL для
// того, чтобы я не разделял строку на слова
// средствами STL. Я этого не сделал.
// Из STL я использовал std::string,
// std::copy и std::back_inserter. Последние два средства
// можно заменить одним циклом, поэтому я посчитал этот
// момент несущественным.
// ------------------------------------------------------

#include <iostream>
#include <cassert>
#include <cstring>

// ------------------------------------------------------
// Support code for testing
// ------------------------------------------------------
template <typename T1, typename T2>
bool require_equal_impl(char const * _str, T1 _l, T2 _r)
{
    if (_l != _r)
    {
        std::cerr << _l << " != " << _r << "\n";
    }
    return (_l == _r);
}
#define EQUAL(_l, _r) assert( require_equal_impl(#_l " == " #_r ,_l, _r));


// ------------------------------------------------------
//  Reverse words in a string.
//
//  Behaviour:
//    - Delimeters ommited: at the end, at the beginning
//    - Serial delimiters are treated as single one
//    - Only one splitting symbol is supported
// ------------------------------------------------------
std::string reverse_words_order( std::string const & _original, char _delimiter )
{
    std::string result;
    if (!_original.size())
        return result;
    
    // Trim extra delimiters at the end and at the beginning
    auto begin = _original.begin();
    while ( (begin != _original.end()) && (*begin == _delimiter))
        ++begin;
    
    auto end = _original.end();
    while ( (end != begin) && (*(end - 1) == _delimiter))
        --end;
    
    // Detect words and push them to result string
    auto nearest_delimiter_pos = end;
    for (auto it = nearest_delimiter_pos - 1; it >= begin; --it)
    {
        if ((*it == _delimiter) || (it == begin) )
        {
            auto word_begin = it + (*it == _delimiter);
            auto word_end = nearest_delimiter_pos;
            nearest_delimiter_pos = it;
            if (word_begin == word_end)
                continue;
            
            std::copy( word_begin, word_end, std::back_inserter(result) );
            // Insert delimiter unless this is the last word
            if (it != begin)
                result += *it;
        }
    }
    return result;
}

// ------------------------------------------------------
// Tests
// ------------------------------------------------------
int main(int argc, const char * argv[]) {
    
    EQUAL("", reverse_words_order("", ' '));
    EQUAL("", reverse_words_order("            ", ' '));
    EQUAL("a", reverse_words_order("a", ' '));
    EQUAL("a", reverse_words_order("a", ' '));
    EQUAL("a", reverse_words_order(" a", ' '));
    EQUAL("a", reverse_words_order("a ", ' '));
    EQUAL("a", reverse_words_order("a  ", ' '));
    EQUAL("a", reverse_words_order("  a  ", ' '));
    EQUAL("a b", reverse_words_order("  b  a  ", ' '));
    EQUAL("a b c", reverse_words_order(" c    b  a  ", ' '));
    EQUAL("string a is This", reverse_words_order("This is a string", ' '));
    
    std::cout << "*** SUCCESS ***" << std::endl;
    return 0;
}
