//
//  main.cpp
//  Ascon
//
//  Created by Alexey Klimov on 08.08.16.
//  Copyright © 2016 Alexey Klimov. All rights reserved.
// ------------------------------------------------------------------------------
// Для решения задачи нужно узнать есть ли второй другой элемент в массиве.
// Это означает, что необходимо выполнить O(n) операций поиска
// в исходном массиве.
// Есть два способа выполнить задачу:
// - Отсортировать массив и найти последовательности
//   дубликатов, при этом удаляя последовательности длиной 1.
//   В этом случае будет затрачено O(1) памяти,
//   не считая памяти исходного массива, и выполнено O(n*logn) операций.
//
// - Оптимизировать операцию поиска. Наилучшее
//   асимтотическое время поиска достигается в хештаблице.
//   Соответственно для хранения таблицы нужно дополнительно
//   выделить O(n) памяти и заполнить таблицу за O(n) времени.
//   Удалить уникальные элементы будет стоить еще O(n) времени.
//
//   Итого:
//     - Первое решение - O(1) памяти и O(n*logn) времени
//     - Второе решение - O(n) памяти и O(n) времени
//
//   Я реализовал второй вариант.
//
//   Решение сделано для любых типов, поддерживающих операцию сравнения.
//   В частном случае, если тип - целочисленный и значения достаточно малы
//   может быть применена сортировка подсчетом.
// ------------------------------------------------------------------------------

#include <iostream>
#include <vector>
#include <unordered_map>
#include <algorithm>
#include <cassert>

// ------------------------------------------------------
// Support code for testing
// ------------------------------------------------------
template <typename T1, typename T2>
bool require_equal_impl(char const * _str, T1 _l, T2 _r)
{
    if (_l != _r)
    {
        std::cerr << _l << " != " << _r << "\n";
    }
    return (_l == _r);
}
#define EQUAL(_l, _r) assert( require_equal_impl(#_l " == " #_r ,_l, _r));


// ------------------------------------------------------
// Find count of each duplicated element. Doesn't include
// unique elements.
// ------------------------------------------------------
template <typename T>
std::unordered_map<T, size_t>
find_duplicates(std::vector<T> & _array)
{
    std::unordered_map<T, size_t> result;
    
    // Count duplicates
    for (auto & i : _array)
        result[i] += 1;
    
    // Remove unique elements
    for(auto it = result.begin(); it != result.end(); )
    {
        if(it->second == 1)
            it = result.erase(it);
        else
            ++it;
    }
    
    return result;
}

// ------------------------------------------------------
// Convinience function, which constructing vector from
// initializer list and running actual find_duplicates on
// it
// ------------------------------------------------------
template <typename T>
std::unordered_map<T, size_t>
find_duplicates(std::initializer_list<T> _init_list)
{
    std::vector<T> array( _init_list );
    return find_duplicates( array );
}

// ------------------------------------------------------
// Tests
// ------------------------------------------------------
int main(int argc, const char * argv[])
{
    {
        auto duplicates = find_duplicates<int>({});
        EQUAL(duplicates.size(), 0);
    }

    {
        auto duplicates = find_duplicates({1});
        EQUAL(duplicates.size(), 0);
    }
    
    {
        auto duplicates = find_duplicates({1, 1});
        EQUAL(duplicates.size(), 1);
        EQUAL(duplicates[1], 2);
    }
    
    {
        auto duplicates = find_duplicates({1, 1, 3, 3, 5});
        EQUAL(duplicates.size(), 2);
        EQUAL(duplicates[1], 2);
        EQUAL(duplicates[3], 2);
    }
    
    std::cout << "*** SUCCESS ***" << std::endl;
    return 0;
}
