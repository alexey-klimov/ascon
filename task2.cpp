//
//  main.cpp
//  Ascon
//
//  Created by Alexey Klimov on 08.08.16.
//  Copyright © 2016 Alexey Klimov. All rights reserved.
// ------------------------------------------------------
// Задача решается за время O(n). Циклический список
// для алгоритма предаставлен в виде итератора-адаптера,
// который преобразует встроенный итератор std::forward_list
// к итератору циклического списка.
// Предполагается, что пустой список на входе алгоритма
// является некорректными входными данными.
// ------------------------------------------------------

#include <iostream>
#include <forward_list>
#include <cassert>

// ------------------------------------------------------
// Support code for testing
// ------------------------------------------------------
template <typename T1, typename T2>
bool require_equal_impl(char const * _str, T1 _l, T2 _r)
{
    if (_l != _r)
    {
        std::cerr << _l << " != " << _r << "\n";
    }
    return (_l == _r);
}
#define EQUAL(_l, _r) assert( require_equal_impl(#_l " == " #_r ,_l, _r));


// ------------------------------------------------------
// Adapter to list iterator that makes std::forward_list
// look like circular sigle-linked list.
// ------------------------------------------------------

template <class t_list>
class circular_list_iterator_adapter
{
    using list_iterator = typename t_list::const_iterator;
    
    list_iterator current;
    list_iterator begin;
    list_iterator end;
    
public:
    circular_list_iterator_adapter(list_iterator _begin, list_iterator _end)
        : current(_begin)
        , begin(_begin)
        , end(_end)
    {}
    circular_list_iterator_adapter(circular_list_iterator_adapter const&) = default;
    circular_list_iterator_adapter(circular_list_iterator_adapter &&) = default;
    circular_list_iterator_adapter& operator= (circular_list_iterator_adapter const &) = default;
    circular_list_iterator_adapter& operator= (circular_list_iterator_adapter &&) = default;
    
    circular_list_iterator_adapter & operator ++()
    {
        ++current;
        if(current == end)
            current = begin;
        return *this;
    }
    
    typename t_list::const_reference operator*()
    {
        return *current;
    }
};


// ------------------------------------------------------
// Types
// ------------------------------------------------------
using circular_list = std::forward_list<int>;
using circular_list_iterator = circular_list_iterator_adapter< circular_list >;


// ------------------------------------------------------
// Actual implementation of finding minimal element in
// circular list.
// ------------------------------------------------------
int find_min( circular_list_iterator _it )
{
    int current_value = *_it;
    
    while ( current_value < *(++_it) )
        current_value = *_it;
    
    return *_it;
}


// ------------------------------------------------------
// Convinience function, which constructing circular list
// from initializer list and running actual find_min on it
// ------------------------------------------------------
int find_min(std::initializer_list<int> _initializer_list)
{
    circular_list list = _initializer_list;
    if (list.empty())
        throw std::logic_error("List shouldn't be empty");
    return find_min(
        circular_list_iterator( list.begin(), list.end() )
    );
}

// ------------------------------------------------------
// Tests
// ------------------------------------------------------
int main(int argc, const char * argv[])
{
    //EQUAL(0, find_min({}) ); - Forbidden
    EQUAL(1, find_min({1}) );
    EQUAL(1, find_min({1, 2}) );
    EQUAL(1, find_min({2, 1}) );
    EQUAL(0, find_min({0, 1, 2, 4, 5}) );
    EQUAL(-5, find_min({0, 1, 2, -5, -4}) );
    
    std::cout << "*** SUCCESS ***" << std::endl;
    return 0;
}
